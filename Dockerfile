FROM python:3.13.2-slim-bookworm
WORKDIR /data
COPY . /data
RUN pip install -r requirements.txt
EXPOSE 5000
ENV NAME country
CMD ["python", "app.py"]




